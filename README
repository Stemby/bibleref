LaTeX Class File : bibleref

Author        : Nicola Talbot (inactive) and Maïeul Rouquette (still active)

Files         : bibleref.dtx   - documented source file
                bibleref.ins   - installation script

LPPL 1.3  https://www.ctan.org/license/lppl1.3
Feature requests: https://framagit.org/maieul/bibleref/issues


The package file bibleref.sty can be used to ensure
consistent formatting of bible citations. Also comes with
bibleref-xidx.sty for extended bibleref indexing functions.

INSTALLATION

To extract the code do:

latex bibleref.ins

Put bibleref somewhere on TeX's path and refresh the database.

To extract the documentation do:

latex bibleref.dtx

A sample file, sample.tex, is provided. This will be extracted
along with the style file bibleref.sty.

REQUIREMENTS

ifthen
fmtcount
amsgen

This material is subject to the LaTeX Project Public License.
See  https://www.latex-project.org/lppl/ for the details of that license.
